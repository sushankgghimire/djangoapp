from django.shortcuts import render, redirect, HttpResponse
from .forms import *
from .models import *
from .filters import *



def country_form(request, pkid=0):
    if request.method == "GET":
        if pkid == 0:
            form = CountryForm()
        else:
            country = Country.objects.get(pk=pkid)
            form = CountryForm(instance=country)
        return render(request, "data_handler/country_form.html", {'form': form})
    else:
        if pkid == 0:
            form = CountryForm(request.POST)
        else:
            country = Country.objects.get(pk=pkid)
            form = CountryForm(request.POST, instance=country)
        if form.is_valid():
            form.save()
        return redirect('/admin/country')


def country_list(request):
    country = Country.objects.all()
    my_filter = CountryFilter(request.GET, queryset=country)
    country = my_filter.qs
    context = {'country_list': country, 'my_filter': my_filter}
    return render(request, "data_handler/country_list.html", context)


def country_delete(request, id):
    country = Country.objects.get(pk=id)
    country.delete()
    return redirect('/admin/country')


def province_form(request, pkid=0):
    if request.method == "GET":
        if pkid == 0:
            form = ProvinceForm()
        else:
            province = Province.objects.get(pk=pkid)
            form = ProvinceForm(instance=province)
        return render(request, "data_handler/province_form.html", {'form': form})
    else:
        if pkid == 0:
            form = ProvinceForm(request.POST)
        else:
            province = Province.objects.get(pk=pkid)
            form = ProvinceForm(request.POST, instance=province)
        if form.is_valid():
            form.save()
        return redirect('/province')


def province_list(request):
    province = Province.objects.all()
    my_filter = ProvinceFilter(request.GET, queryset=province)
    province = my_filter.qs
    context = {'province_list': province, 'my_filter': my_filter}
    return render(request, "data_handler/province_list.html", context)


def province_delete(request, id):
    province = Province.objects.get(pk=id)
    province.delete()
    return redirect('/admin/province')


def district_form(request, pkid=0):
    if request.method == "GET":
        if pkid == 0:
            form = DistrictForm()
        else:
            district = District.objects.get(pk=pkid)
            form = DistrictForm(instance=district)
        return render(request, "data_handler/district_form.html", {'form': form})
    else:
        if pkid == 0:
            form = DistrictForm(request.POST)
        else:
            district = District.objects.get(pk=pkid)
            form = DistrictForm(request.POST, instance=district)
        if form.is_valid():
            form.save()
        return redirect('/admin/district')


def district_list(request):
    district = District.objects.all()
    my_filter = DistrictFilter(request.GET, queryset=district)
    district = my_filter.qs
    context = {'district_list': district, 'my_filter': my_filter}
    return render(request, "data_handler/district_list.html", context)


def district_delete(request, id):
    district = District.objects.get(pk=id)
    district.delete()
    return redirect('/admin/district')


def local_level_type_form(request, pkid=0):
    if request.method == "GET":
        if pkid == 0:
            form = LocalLevelTypeForm()
        else:
            local_level_type = LocalLevelType.objects.get(pk=pkid)
            form = LocalLevelTypeForm(instance=local_level_type)
        return render(request, "data_handler/local_level_type_form.html", {'form': form})
    else:
        if pkid == 0:
            form = LocalLevelTypeForm(request.POST)
        else:
            local_level_type = LocalLevelType.objects.get(pk=pkid)
            form = LocalLevelTypeForm(request.POST, instance=local_level_type)
        if form.is_valid():
            form.save()
        return redirect('/admin/local_level_type')


def local_level_type_list(request):
    local_level_type = LocalLevelType.objects.all()
    my_filter = LocalLevelTypeFilter(request.GET, queryset=local_level_type)
    local_level_type = my_filter.qs
    context = {'local_level_type_list': local_level_type, 'my_filter': my_filter}
    return render(request, "data_handler/local_level_type_list.html", context)


def local_level_type_delete(request, id):
    local_level_type = LocalLevelType.objects.get(pk=id)
    local_level_type.delete()
    return redirect('/admin/local_level_type')


def local_level_form(request, pkid=0):
    if request.method == "GET":
        if pkid == 0:
            form = LocalLevelForm()
        else:
            local_level = LocalLevel.objects.get(pk=pkid)
            form = LocalLevelForm(instance=local_level)
        return render(request, "data_handler/local_level_form.html", {'form': form})
    else:
        if pkid == 0:
            form = LocalLevelForm(request.POST)
        else:
            local_level = LocalLevel.objects.get(pk=pkid)
            form = LocalLevelForm(request.POST, instance=local_level)
        if form.is_valid():
            form.save()
        return redirect('/admin/local_level')


def local_level_list(request):
    local_level = LocalLevel.objects.all()
    my_filter = LocalLevelFilter(request.GET, queryset=local_level)
    local_level = my_filter.qs
    context = {'local_level_list': local_level, 'my_filter': my_filter}
    return render(request, "data_handler/local_level_list.html", context)


def local_level_delete(request, id):
    local_level = LocalLevel.objects.get(pk=id)
    local_level.delete()
    return redirect('/admin/local_level')


def fiscal_year_form(request, pkid=0):
    if request.method == "GET":
        if pkid == 0:
            form = FiscalYearForm()
        else:
            fiscal_year = FiscalYear.objects.get(pk=pkid)
            form = FiscalYearForm(instance=fiscal_year)
        return render(request, "data_handler/fiscal_year_form.html", {'form': form})
    else:
        if pkid == 0:
            form = FiscalYearForm(request.POST)
        else:
            fiscal_year = FiscalYear.objects.get(pk=pkid)
            form = FiscalYearForm(request.POST, instance=fiscal_year)
        if form.is_valid():
            form.save()
        return redirect('/admin/fiscal_year')


def fiscal_year_list(request):
    fiscal_year = FiscalYear.objects.all()
    my_filter = FiscalYearFilter(request.GET, queryset=fiscal_year)
    fiscal_year = my_filter.qs
    context = {'fiscal_year_list': fiscal_year, 'my_filter': my_filter}
    return render(request, "data_handler/fiscal_year_list.html", context)


def fiscal_year_delete(request, id):
    fiscal_year = FiscalYear.objects.get(pk=id)
    fiscal_year.delete()
    return redirect('/admin/fiscal_year')


def month_form(request, pkid=0):
    if request.method == "GET":
        if pkid == 0:
            form = MonthForm()
        else:
            month = Month.objects.get(pk=pkid)
            form = MonthForm(instance=month)
        return render(request, "data_handler/month_form.html", {'form': form})
    else:
        if pkid == 0:
            form = MonthForm(request.POST)
        else:
            month = Month.objects.get(pk=pkid)
            form = MonthForm(request.POST, instance=month)
        if form.is_valid():
            form.save()
        return redirect('/admin/month')


def month_list(request):
    month = Month.objects.all()
    my_filter = MonthFilter(request.GET, queryset=month)
    month = my_filter.qs
    context = {'month_list': month, 'my_filter': my_filter}
    return render(request, "data_handler/month_list.html", context)


def month_delete(request, id):
    month = Month.objects.get(pk=id)
    month.delete()
    return redirect('/admin/month')


def gender_form(request, pkid=0):
    if request.method == "GET":
        if pkid == 0:
            form = GenderForm()
        else:
            gender = Gender.objects.get(pk=pkid)
            form = GenderForm(instance=gender)
        return render(request, "data_handler/gender_form.html", {'form': form})
    else:
        if pkid == 0:
            form = GenderForm(request.POST)
        else:
            gender = Gender.objects.get(pk=pkid)
            form = GenderForm(request.POST, instance=gender)
        if form.is_valid():
            form.save()
        return redirect('/admin/gender')


def gender_list(request):
    gender = Gender.objects.all()
    my_filter = GenderFilter(request.GET, queryset=gender)
    gender = my_filter.qs
    context = {'gender_list': gender, 'my_filter': my_filter}
    return render(request, "data_handler/gender_list.html", context)


def gender_delete(request, id):
    gender = Gender.objects.get(pk=id)
    gender.delete()
    return redirect('/admin/gender')


def privileged_form(request, pkid=0):
    if request.method == "GET":
        if pkid == 0:
            form = PrivilegedForm()
        else:
            privileged = Privileged.objects.get(pk=pkid)
            form = PrivilegedForm(instance=privileged)
        return render(request, "data_handler/privileged_form.html", {'form': form})
    else:
        if pkid == 0:
            form = PrivilegedForm(request.POST)
        else:
            privileged = Privileged.objects.get(pk=pkid)
            form = PrivilegedForm(request.POST, instance=privileged)
        if form.is_valid():
            form.save()
        return redirect('/admin/privileged')


def privileged_list(request):
    privileged = Privileged.objects.all()
    my_filter = PrivilegedFilter(request.GET, queryset=privileged)
    privileged = my_filter.qs
    context = {'privileged_list': privileged, 'my_filter': my_filter}
    return render(request, "data_handler/privileged_list.html", context)


def privileged_delete(request, id):
    privileged = Privileged.objects.get(pk=id)
    privileged.delete()
    return redirect('/admin/privileged')


def homepage(request):
    template = "data_handler/homepage.html"
    context = {}
    return render (request,template,context)


