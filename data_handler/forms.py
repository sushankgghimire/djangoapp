from django import forms
from .models import *


class CountryForm(forms.ModelForm):
    class Meta:
        model = Country
        fields = '__all__'
        labels = {
            'name_en': 'Name',
            'name_lc': 'नाम',
            'code': 'Code'
        }


class ProvinceForm(forms.ModelForm):
    class Meta:
        model = Province
        fields = '__all__'
        labels = {
            'country': 'देश',
            'name_en': 'Name',
            'name_lc': 'नाम',
            'code': 'Code',
        }

    def __init__(self, *args, **kwargs):
        super(ProvinceForm, self).__init__(*args, **kwargs)
        self.fields['country'].empty_label = "देश छान्नुहोस्"


class DistrictForm(forms.ModelForm):
    class Meta:
        model = District
        fields = '__all__'
        labels = {
            'province': 'प्रदेश',
            'name_en': 'Name',
            'name_lc': 'नाम',
            'code': 'Code',
        }

    def __init__(self, *args, **kwargs):
        super(DistrictForm, self).__init__(*args, **kwargs)
        self.fields['province'].empty_label = "प्रदेश छान्नुहोस्"


class LocalLevelTypeForm(forms.ModelForm):
    class Meta:
        model = LocalLevelType
        fields = '__all__'
        labels = {
            'name_en': 'Name',
            'name_lc': 'नाम',
            'code': 'Code',
        }


class LocalLevelForm(forms.ModelForm):
    class Meta:
        model = LocalLevel
        fields = '__all__'
        labels = {
            'local_level_type': 'स्थानीय तह प्रकार',
            'district': 'जिल्ला',
            'name_en': 'Name',
            'name_lc': 'नाम',
            'code': 'Code',
        }

    def __init__(self, *args, **kwargs):
        super(LocalLevelForm, self).__init__(*args, **kwargs)
        self.fields['province'].empty_label = "प्रदेश छान्नुहोस्"
        self.fields['district'].empty_label = "जिल्ला छान्नुहोस्"


class FiscalYearForm(forms.ModelForm):
    class Meta:
        model = FiscalYear
        fields = '__all__'



class MonthForm(forms.ModelForm):
    class Meta:
        model = Country
        fields = '__all__'
        labels = {
            'name_en': 'Month',
            'name_lc': 'महिना',
            'code': 'Code'
        }


class GenderForm(forms.ModelForm):
    class Meta:
        model = Gender
        fields = '__all__'
        labels = {
            'name_en': 'Gender',
            'name_lc': 'लिङ्ग',
        }


class PrivilegedForm(forms.ModelForm):
    class Meta:
        model = Privileged
        fields = '__all__'
        labels = {
            'name_en': 'Inclusive Group Name',
            'name_lc': 'समावेशी समूहको नाम',
        }