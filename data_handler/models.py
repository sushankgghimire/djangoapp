from django.db import models
from nepali_date import NepaliDate


class Country(models.Model):
    code = models.BigIntegerField()
    name_en = models.CharField(max_length=25)
    name_lc = models.CharField(max_length=25)

    def __str__(self):
        return self.name_lc


class Province(models.Model):
    code = models.BigIntegerField()
    name_en = models.CharField(max_length=25)
    name_lc = models.CharField(max_length=25)
    country = models.ForeignKey(Country, on_delete=models.CASCADE)

    def __str__(self):
        return self.name_lc


class District(models.Model):
    code = models.BigIntegerField()
    name_en = models.CharField(max_length=25)
    name_lc = models.CharField(max_length=25)
    province = models.ForeignKey(Province, on_delete=models.CASCADE)

    def __str__(self):
        return self.name_lc


class LocalLevelType(models.Model):
    code = models.BigIntegerField()
    name_en = models.CharField(max_length=25)
    name_lc = models.CharField(max_length=25)

    def __str__(self):
        return self.name_lc


class LocalLevel(models.Model):
    code = models.BigIntegerField()
    name_en = models.CharField(max_length=50)
    name_lc = models.CharField(max_length=50)
    district = models.ForeignKey(District, on_delete=models.CASCADE)
    local_level_type = models.ForeignKey(LocalLevelType, on_delete=models.CASCADE)


class FiscalYear(models.Model):
    code = models.CharField(max_length=15)
    start_date_ad = models.DateField()
    end_date_ad = models.DateField()
    start_date_bs = models.DateField()
    end_date_bs = models.DateField()


class Month(models.Model):
    code = models.BigIntegerField()
    name_en = models.CharField(max_length=20)
    name_lc = models.CharField(max_length=20)


class Gender(models.Model):
    name_en = models.CharField(max_length=20)
    name_lc = models.CharField(max_length=20)


class Privileged(models.Model):
    name_en = models.CharField(max_length=20)
    name_lc = models.CharField(max_length=20)

