from django.urls import path

from . import views

urlpatterns = [
    path('homepage', views.homepage, name="homepage"),
    path('country/new', views.country_form, name="country_add"),
    path('country/new/<int:id>/', views.country_form, name="country_update"),
    path('country/', views.country_list, name="country_list"),
    path('country/delete/<int:id>', views.country_delete, name="country_delete"),

    path('province/new', views.province_form, name="province_add"),
    path('province/new/<int:id>/', views.province_form, name="province_update"),
    path('province/', views.province_list, name="province_list"),
    path('province/delete/<int:id>', views.province_delete, name="province_delete"),

    path('district/new', views.district_form, name="district_add"),
    path('district/new/<int:id>/', views.district_form, name="district_update"),
    path('district/', views.district_list, name="district_list"),
    path('district/delete/<int:id>', views.district_delete, name="district_delete"),

    path('local_level_type/new', views.local_level_type_form, name="local_level_type_add"),
    path('local_level_type/new/<int:id>/', views.local_level_type_form, name="local_level_type_update"),
    path('local_level_type/', views.local_level_type_list, name="local_level_type_list"),
    path('local_level_type/delete/<int:id>', views.local_level_type_delete, name="local_level_type_delete"),

    path('local_level/new', views.local_level_form, name="local_level_add"),
    path('local_level/new/<int:id>/', views.local_level_form, name="local_level_update"),
    path('local_level/', views.local_level_list, name="local_level_list"),
    path('local_level/delete/<int:id>', views.local_level_delete, name="local_level_delete"),

    path('fiscal_year/new', views.fiscal_year_form, name="fiscal_year_add"),
    path('fiscal_year/new/<int:id>/', views.fiscal_year_form, name="fiscal_year_update"),
    path('fiscal_year/', views.fiscal_year_list, name="fiscal_year_list"),
    path('fiscal_year/delete/<int:id>', views.fiscal_year_delete, name="fiscal_year_delete"),

    path('month/new', views.month_form, name="month_add"),
    path('month/new/<int:id>/', views.month_form, name="month_update"),
    path('month/', views.month_list, name="month_list"),
    path('month/delete/<int:id>', views.month_delete, name="month_delete"),

    path('gender/new', views.gender_form, name="gender_add"),
    path('gender/new/<int:id>/', views.gender_form, name="gender_update"),
    path('gender/', views.gender_list, name="gender_list"),
    path('gender/delete/<int:id>', views.gender_delete, name="gender_delete"),

    path('privileged/new', views.privileged_form, name="privileged_add"),
    path('privileged/new/<int:id>/', views.privileged_form, name="privileged_update"),
    path('privileged/', views.privileged_list, name="privileged_list"),
    path('privileged/delete/<int:id>', views.privileged_delete, name="privileged_delete"),
]
