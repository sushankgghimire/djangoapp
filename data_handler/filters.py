import django_filters
from django_filters import  CharFilter
from .models import *


class CountryFilter(django_filters.FilterSet):
    name_en = CharFilter(field_name='name_en', lookup_expr='icontains', label='Country')
    name_lc = CharFilter(field_name='name_lc', lookup_expr='icontains', label='देश')

    class Meta:
        model = Country
        fields = '__all__'



class ProvinceFilter(django_filters.FilterSet):
    name_en = CharFilter(field_name='name_en', lookup_expr='icontains', label='Province')
    name_lc = CharFilter(field_name='name_lc', lookup_expr='icontains', label='प्रदेश')
    class Meta:
        model = Province
        fields = '__all__'


class DistrictFilter(django_filters.FilterSet):
    name_en = CharFilter(field_name='name_en', lookup_expr='icontains', label='District')
    name_lc = CharFilter(field_name='name_lc', lookup_expr='icontains', label='जिल्ला')
    name_en = CharFilter(field_name='province', lookup_expr='icontains', label='Province')
    class Meta:
        model = District
        fields = '__all__'


class LocalLevelTypeFilter(django_filters.FilterSet):
    name_en = CharFilter(field_name='name_en', lookup_expr='icontains', label='Name')
    name_lc = CharFilter(field_name='name_lc', lookup_expr='icontains', label='नाम')
    class Meta:
        model = LocalLevelType
        fields = '__all__'


class LocalLevelFilter(django_filters.FilterSet):
    name_en = CharFilter(field_name='name_en', lookup_expr='icontains', label='Name')
    name_lc = CharFilter(field_name='name_lc', lookup_expr='icontains', label='नाम')
    class Meta:
        model = LocalLevel
        fields = '__all__'


class FiscalYearFilter(django_filters.FilterSet):

    class Meta:
        model = FiscalYear
        fields = ['code']


class MonthFilter(django_filters.FilterSet):
    name_en = CharFilter(field_name='name_en', lookup_expr='icontains', label='Month')
    name_lc = CharFilter(field_name='name_lc', lookup_expr='icontains', label='महिना')
    class Meta:
        model = Month
        fields = '__all__'


class GenderFilter(django_filters.FilterSet):
    name_en = CharFilter(field_name='name_en', lookup_expr='icontains', label='Gender')
    name_lc = CharFilter(field_name='name_lc', lookup_expr='icontains', label='लिङ्ग')
    class Meta:
        model = Gender
        fields = '__all__'


class PrivilegedFilter(django_filters.FilterSet):
    name_en = CharFilter(field_name='name_en', lookup_expr='icontains', label='Name')
    name_lc = CharFilter(field_name='name_lc', lookup_expr='icontains', label='नाम')
    class Meta:
        model = Privileged
        fields = '__all__'